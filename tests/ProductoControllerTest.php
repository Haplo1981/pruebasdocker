<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductoControllerTest extends WebTestCase
{
    public function testNuevoProducto()
    {
        $cliente = static::createClient();
        /** @var KernelBrowser $cliente */

        /* No se envía HEADER con la autorizacion */
        $cliente->request('POST','/api/productos');
        $this->assertResponseStatusCodeSame('401');

        /* Se envía autorizacion, datos vacíos */
        $cliente->request('POST','/api/productos',[],[],['HTTP_AUTHORIZATION'=>'Bearer admintoken']);
        $this->assertResponseStatusCodeSame('400');

        /* Se envia autorizacion, datos válidos */
        $cliente->jsonRequest('POST','/api/productos',[
            "nombre"        => "Producto4_test",
            "descripcion"   => "Producto de prueba PHP-UNIT",
            "precio"        => "6.5",
            "tipoIva"       => "10",
        ],['HTTP_AUTHORIZATION'=>'Bearer admintoken']);
        $this->assertResponseStatusCodeSame('200');

        /* Se envia autorización, IVA no válido*/
        $cliente->jsonRequest('POST','/api/productos',[
            "nombre"        => "Producto4_test",
            "descripcion"   => "Producto de prueba PHP-UNIT",
            "precio"        => "6.5",
            "tipoIva"       => "xxx",
        ],['HTTP_AUTHORIZATION'=>'Bearer admintoken']);
        $respuesta=$cliente->getResponse();
        $this->assertResponseStatusCodeSame('400');
        $this->assertJsonStringEqualsJsonString('{"error": "Tipo de iva no valido"}',$respuesta->getContent());

        /* Se envia autorización, Precio no válido*/
        $cliente->jsonRequest('POST','/api/productos',[
            "nombre"        => "Producto4_test",
            "descripcion"   => "Producto de prueba PHP-UNIT",
            "precio"        => "xxx",
            "tipoIva"       => "10",
        ],['HTTP_AUTHORIZATION'=>'Bearer admintoken']);
        $respuesta=$cliente->getResponse();
        $this->assertResponseStatusCodeSame('400');
        $this->assertJsonStringEqualsJsonString('{"error": "Formato de precio no válido"}',$respuesta->getContent());

        /* Se envia autorización, Nombre vacío*/
        $cliente->jsonRequest('POST','/api/productos',[
            "nombre"        => "",
            "descripcion"   => "Producto de prueba PHP-UNIT",
            "precio"        => "15.0",
            "tipoIva"       => "10",
        ],['HTTP_AUTHORIZATION'=>'Bearer admintoken']);
        $respuesta=$cliente->getResponse();
        $this->assertResponseStatusCodeSame('400');
        $this->assertJsonStringEqualsJsonString('{"error": "Falta algún dato obligatorio"}',$respuesta->getContent());
    }
}