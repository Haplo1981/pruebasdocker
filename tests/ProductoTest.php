<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Producto;

class ProductoTest extends ApiTestCase
{
    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET','/api/productos',[
            'headers' => [
                'Authorization' => 'Bearer admintoken',
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type','application/ld+json; charset=utf-8');
        
    }

    // public function testNuevoProducto(): void
    // {
    //     $response = static::createClient()->request('GET','/api/productos',[
    //         'headers' => [
    //             'Authorization' => 'Bearer admintoken',
    //         ],
    //         'body' => [
    //             "nombre"        => "Producto4_test",
    //             "descripcion"   => "Producto de prueba PHP-UNIT",
    //             "precio"        => "6.5",
    //             "tipoIva"       => "10d",
    //         ]
    //     ]);

    //     $this->assertResponseIsSuccessful();
    //     $this->assertResponseStatusCodeSame('200');
    // }
}
