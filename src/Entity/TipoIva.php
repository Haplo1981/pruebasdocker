<?php

namespace App\Entity;

use App\Repository\TipoIvaRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TipoIvaRepository::class)
 * @ApiResource(
 *  collectionOperations={},
 *  itemOperations={"get"}
 * )
 */
class TipoIva
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Groups({"read"})
     */
    private $valorIva;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValorIva(): ?string
    {
        return $this->valorIva;
    }

    public function setValorIva(string $valorIva): self
    {
        $this->valorIva = $valorIva;

        return $this;
    }
}
