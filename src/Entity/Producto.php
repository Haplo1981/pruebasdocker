<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ProductoRepository;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ProductoController;
use App\Entity\TipoIva;


/**
 * @ORM\Entity(repositoryClass=ProductoRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups" = {"read"}},
 *      denormalizationContext={"groups" = {"write"}},
 *      collectionOperations={
            "get",
 *          "nuevoProducto" = {
 *              "method" = "POST",
 *              "path" = "/productos",
 *              "controller" = ProductoController::class,
 *              "openapi_context" = {
 *                   "summary"    = "Crea un nuevo producto",
 *                   "description" = "Crea un nuevo producto",
 *              },
 *              "read" = false,
 *          }},
 *      itemOperations={"get"}
 * )
 
 */
class Producto implements JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read","write"})
     * @ApiFilter(SearchFilter::class, properties={"nombre":"exact"})
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"read","write"})
     */
    private $descripcion;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"read","write"})
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity=TipoIva::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"read","write"})
     * @ApiProperty(
     *      attributes={
     *          "openapi_context"={
     *              "type"="number"
     *          }
     *      }
     * )
     */
     
    private $tipoIva;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"read"})
     */
    private $pvp;

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    public function setPrecio(string $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getTipoIva(): ?TipoIva
    {
        return $this->tipoIva;
    }

    public function setTipoIva(?TipoIva $tipoIva): self
    {
        $this->tipoIva = $tipoIva;

        return $this;
    }

    public function getPvp(): ?string
    {
        return $this->pvp;
    }

    public function setPvp(string $pvp): self
    {
        $this->pvp = $pvp;

        return $this;
    }

    public function jsonSerialize()
    {
        $jsonData  = get_object_vars($this);
         $jsonData['tipoIva'] = [
            "id"=>$this->getTipoIva()->getId(),
            "valorIva"=>$this->getTipoIva()->getValorIva()
        ]; //Para mostrar la respuesta correctamente
         
        return $jsonData;
    }
    
}
