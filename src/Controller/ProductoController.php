<?php

namespace App\Controller;

use App\Entity\Producto;
use App\Entity\TipoIva;
use App\Repository\ProductoRepository;
use App\Repository\TipoIvaRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* Class ProductoController
* @package App\Controller
*
* @Route(path="/api/")
*/
class ProductoController extends AbstractController{

    private $ProductosRepositorio;
    /** @var ProductoRepository $ProductosRepositorio  */
    private $TiposIvaRepositorio;
    /** @var TipoIvaRepository $TiposIvaRepositorio */

    public function __construct(ProductoRepository $ProductosRepositorio, TipoIvaRepository $TiposIvaRepositorio)
    {
        $this->ProductosRepositorio = $ProductosRepositorio;
        $this->TiposIvaRepositorio = $TiposIvaRepositorio;
    }

    /**
     * Nuevo Producto.
     * Crea un nuevo producto en la Base de Datos
     * 
     * @param Request $request Petición
     * @return JsonResponse Respuesta
     * @Route("productos",name="nuevo_producto",methods={"POST"})
     */
    public function NuevoProducto(Request $request): JsonResponse
    {
        
        // Comprobar si está la cabecera de autorización válida
       
        if(null===$request->headers->get('Authorization') || $request->headers->get('Authorization')!= 'Bearer admintoken'){
            return new JsonResponse('{"error": "No autorizado"}',401,[],true);
        }

        $data = json_decode($request->getContent(),true);
        
        if(empty($data)){
            return new JsonResponse('{"error": "No ha enviado datos"}',400,[],true);
        }

        $nombre=empty($data['nombre'])?'':$data['nombre'];
        $precio=empty($data['precio'])?'':$data['precio'];
        if(!is_numeric($precio)){
            return new JsonResponse('{"error": "Formato de precio no válido"}',400,[],true);
        }
        $descripcion=empty($data['descripcion'])?'':$data['descripcion'];
        $desTipoIva = empty($data['tipoIva'])?'':$data['tipoIva'];

        if(empty($nombre) || empty($precio) || empty($descripcion) || empty($desTipoIva)){
            return new JsonResponse('{"error": "Falta algún dato obligatorio"}',400,[],true);
        }

        $tipoIva = $this->TiposIvaRepositorio->findOneByValorIva($desTipoIva);
        /** @var TipoIva $tipoIva */

        if(empty($tipoIva) || !is_numeric($desTipoIva)){
            return new JsonResponse('{"error": "Tipo de iva no valido"}',400,[],true);
        }

        $pvp = $precio * (1+$tipoIva->getValorIva()/100);

        $prod = $this->ProductosRepositorio->guardarProducto($nombre,$descripcion,$precio,$tipoIva,$pvp);
        /** @var Producto $prod */
        $jsonRes = json_encode($prod,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS,3);
        return new JsonResponse($jsonRes,200);
    }
}