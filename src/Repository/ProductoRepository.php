<?php

namespace App\Repository;

use App\Entity\Producto;
use App\Entity\TipoIva;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @method Producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Producto[]    findAll()
 * @method Producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductoRepository extends ServiceEntityRepository
{
    private $manager;
    /** @var EntityManagerInterface $manager */

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Producto::class);
        $this->manager = $manager;
    }

    public function guardarProducto(string $nombre, string $descripcion, float $precio, TipoIva $tipoIva, float $pvp ): Producto
    {

        $prod = new Producto();
        
        $prod->setNombre($nombre);
        $prod->setPrecio($precio);
        $prod->setDescripcion($descripcion);
        $prod->setTipoIva($tipoIva);
        $prod->setPvp($pvp);

        $this->manager->persist($prod);
        $this->manager->flush();

        return $prod;
    }
    // /**
    //  * @return Producto[] Returns an array of Producto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Producto
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
